create database movies;
use movies;
create table comingsoonmovies(movieid int(10)primary key not null,movietitle varchar(45),year int(10),storyline varchar(300),rating int(10),Classification varchar(45));

insert into comingsoonmovies values(1,"pushpa",2021,"Pushpa succeeds in red sandalwood smuggling and finds ways to overpower the police and his rivals",9);
insert into comingsoonmovies values(2,"morbius",2022,"Biochemist Michael Morbius tries to cure himself of a rare blood disease, but when his experiment goes wrong, he inadvertently infects himself with a form of vampirism instead",8);
insert into comingsoonmovies values(3,"RRR",2021,"RRR is an upcoming Indian Telugu-language period action drama film directed by S. S. Rajamouli, and produced by D. V. V. Danayya of DVV Entertainments. The film stars N. T. Rama Rao Jr., Ram Charan",9);
insert into comingsoonmovies values(4,"badhaai do",2022,"Badhaai Do is an upcoming Indian Hindi-language family drama film jointly written by Suman Adhikary and Akshat Ghildial and directed by Harshavardhan Kulkarni. The film produced by Junglee Pictures, that serves as a spiritual sequel of the 2018 film Badhaai Ho",7);
insert into comingsoonmovies values(5,"gangubai kathiawadi",2022,"Gangubai Kathiawadi is an upcoming Indian Hindi-language biographical crime drama film directed by Sanjay Leela Bhansali and produced by Jayantilal Gada and Sanjay Leela Bhansali",8);
select * from comingsoonmovies;

create table moviesintheatres(movieid int(10)primary key not null,movietitle varchar(45),year int(10),storyline varchar(300),rating int(10));

insert into moviesintheatres values(1,"Avengers",2021,"the superiors who run S.H.I.E.L.D., Fury is grilled about his plan to bring about the Avengers Initiative",9);
insert into moviesintheatres values(2,"Annihilation",2021,"A biologist's husband disappears. She puts her name forward for an expedition into an environmental disaster zone, but does not find what she's expecting",8);
insert into moviesintheatres values(3,"Hannah",2021,"Intimate portrait of a woman drifting between reality and denial when she is left alone to grapple with the consequences of her husband's imprisonment",9);
insert into moviesintheatres values(4,"The Lodgers",2021,"Anglo Irish twins Rachel and Edward share a strange existence in their crumbling family estate",7);
insert into moviesintheatres values(5,"Beast of Burden",2021,"Sean Haggerty only has an hour to deliver his illegal cargo",8);
select * from moviesintheatres;

create table topratedindian(movieid int(10)primary key not null,movietitle varchar(45),year int(10),storyline varchar(300),rating int(10));

insert into topratedindian values(1,"The Chamber",2009,"A claustrophobic survival thriller set beneath the Yellow Sea off the coast of North Korea ",7);
insert into topratedindian values(2,"hloo",2007,"Purely about love and it is magical connection between two souls",8);
insert into topratedindian values(3,"Garshana",2006,"In this movie the police officer loves heroine",8);
insert into topratedindian values(4,"prema",2015,"It is about robbery of personal photos of heroine and hero saved heroine from that person",8);
insert into topratedindian values(5,"Varsham",2017,"In this heron and villain fight happens for the herine",9);
select * from topratedindian;

create table topratedmovies(movieid int(10)primary key not null,movietitle varchar(45),year int(10),storyline varchar(300),rating int(10));

insert into topratedmovies values(1,"Bahubali",2020,"Near the ancient Indian kingdom of Mahishmati, an injured woman is seen exiting a cave down by a mountain waterfall carrying an infant",10);
insert into topratedmovies values(2,"sukumarudu",2005,"something happend between two souls",9);
insert into topratedmovies values(3,"Rebel",2008,"It is mainly about fight between hero and villain",9);
insert into topratedmovies values(4,"Srimanthudu",2010,"It is about sacrifice and farming in a village",10);
insert into topratedmovies values(5,"Bale Bale",2009,"about Memory loss of Hero but heroine loves the hero without having fights",9);
 select * from topratedmovies;
 
 create table favouritemovies(movieid int(10)primary key not null,movietitle varchar(45),year int(10),storyline varchar(300),rating int(10));
 
 insert into favouritemovies values(1,"yeh jawani yeh dewani",2008,"In this heroine so innocent by entering hero in heroine life the total story turns into beautiful",9);
 insert into favouritemovies values(2,"chennai Express",2008,"This story happens in the train and the hero is telugu guy but heroine is tamil girl ",9);
 insert into favouritemovies values(3,"Majili",2008,"Hero loves some other girl but she go far way from hero life",8);
 insert into favouritemovies values(4,"Alludu srinu",2014,"The heroine is Richest girl in the movie compared with hero",8);
 insert into favouritemovies values(5,"RadheShayam",2021,"It is a lovely movie and the songs of this movie is fully graphics",9);
 select * from favouritemovies;
 