package com.assignment.movies.factory;

public class FactoryPattern{
		public static Movies setMovieType(String type) {
			if(type.equalsIgnoreCase("ComingSoonMovies")) {
				return new ComingSoonMovies();
			}
			else if(type.equalsIgnoreCase("MoviesInTheatres")) {
				return new MoviesInTheatres();
			}
			else if(type.equalsIgnoreCase("TopRatedMovies")) {
				return new TopRatedMovies();
			}
			else if(type.equalsIgnoreCase("TopRatedIndian")) {
				return new TopRatedIndian();
			}
			else if(type.equalsIgnoreCase("FavouriteMovies")) {
				return new FavouriteMovies();
			}
			return null;
		}
	}


