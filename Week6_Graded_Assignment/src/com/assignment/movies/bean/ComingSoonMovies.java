package com.assignment.movies.bean;


public class ComingSoonMovies{
	
	private int movieId;
	private String movieTitle;
	private int year;
	private String storyLine;
	private int rating;
	
	
	
	
	
	public ComingSoonMovies() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ComingSoonMovies(int movieId, String movieTitle, int year, String storyLine, int rating) {
		super();
		this.movieId = movieId;
		this.movieTitle = movieTitle;
		this.year = year;
		this.storyLine = storyLine;
		this.rating = rating;
		
	}



	public int getMovieId() {
		return movieId;
	}



	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}



	public String getMovieTitle() {
		return movieTitle;
	}



	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}



	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}



	public String getStoryLine() {
		return storyLine;
	}



	public void setStoryLine(String storyLine) {
		this.storyLine = storyLine;
	}



	public int getRating() {
		return rating;
	}



	public void setRating(int rating) {
		this.rating = rating;
	}



	

	
	

	@Override
	public String toString() {
		return "ComingSoonMovies [movieId=" + movieId + ", movieTitle=" + movieTitle + ", year=" + year + ", storyLine="
				+ storyLine + ", rating=" + rating +  "]";
	}

}
