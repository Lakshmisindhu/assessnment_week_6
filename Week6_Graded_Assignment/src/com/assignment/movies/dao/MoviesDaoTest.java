package com.assignment.movies.dao;

import java.util.List;
import org.junit.*;
import com.assignment.movies.bean.*;

public class MoviesDaoTest {

	//@Test
	public void testGetMovieComing() {
		//fail("Not yet implemented");
		MoviesDao md=new MoviesDao();
		List<ComingSoonMovies> listOfComing=md.getMovieComing();
		Assert.assertNotNull(listOfComing);
	    Assert.assertEquals(5, listOfComing.size());
	    return;
	}

	//@Test
	public void testGetMoviesTheatres() {
		//fail("Not yet implemented");
		MoviesDao md=new MoviesDao();
		List<MoviesInTheatres> listOfTheatreMovies=md.getMoviesTheatres();
		Assert.assertNotNull(listOfTheatreMovies);
	    Assert.assertEquals(5, listOfTheatreMovies.size());
	    return;
	}

	//@Test
	public void testGetMoviesTopRated() {
		//fail("Not yet implemented");
		MoviesDao md=new MoviesDao();
		List<TopRatedMovies> listOfTopRated=md.getMoviesTopRated();
		Assert.assertNotNull(listOfTopRated);
	    Assert.assertEquals(5, listOfTopRated.size());
	    return;
	}

	@Test
	public void testGetMoviesIndian() {
		//fail("Not yet implemented");
		MoviesDao md=new MoviesDao();
		List<TopRatedIndian> listOfTopIndian=md.getMoviesIndian();
		Assert.assertNotNull(listOfTopIndian);
	    Assert.assertEquals(5, listOfTopIndian.size());
	    return;
	}

	//@Test
	public void testGetMoviesFavourite() {
		//fail("Not yet implemented");
	}

}
