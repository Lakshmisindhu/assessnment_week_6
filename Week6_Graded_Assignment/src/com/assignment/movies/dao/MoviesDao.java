package com.assignment.movies.dao;

import java.sql.*;
import java.util.*;

import com.assignment.movies.bean.*;

public class MoviesDao {
	 public static Connection con;   
	    public MoviesDao(){
	        if(con == null){
	            String dbUrl = "jdbc:mysql://localhost:3306/movies";
	            String dbClass = "com.mysql.cj.jdbc.Driver";
	            try {
	                Class.forName(dbClass);
	                con = DriverManager.getConnection (dbUrl, "root", "Bsindhu@505");
	            }catch(Exception e) {
	                e.printStackTrace();
	            }
	        }
	    }
	
	
	
	public List<ComingSoonMovies> getMovieComing(){
		
		List<ComingSoonMovies> result = new ArrayList<>();
        String sql = "select * from comingsoonmovies";
        try{
            PreparedStatement ps = con.prepareStatement(sql);
           
            ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ComingSoonMovies cs = new ComingSoonMovies();
			cs.setMovieId(rs.getInt("MovieId"));
			cs.setMovieTitle(rs.getString("MovieTitle"));
			cs.setYear(rs.getInt("Year"));
			cs.setStoryLine(rs.getString("storyline"));
			cs.setRating(rs.getInt("Rating"));
			
			result.add(cs);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
		public List<MoviesInTheatres> getMoviesTheatres(){

			List<MoviesInTheatres> result = new ArrayList<>();
	        String sql = "select * from moviesintheatres";
	        try{
	            PreparedStatement ps = con.prepareStatement(sql);
	           
	            ResultSet rs = ps.executeQuery();
	            while (rs.next()) {
				MoviesInTheatres mt = new MoviesInTheatres();
				mt.setMovieId(rs.getInt("MovieId"));
				mt.setMovieTitle(rs.getString("MovieTitle"));
				mt.setYear(rs.getInt("Year"));
				mt.setStoryLine(rs.getString("storyline"));
				mt.setRating(rs.getInt("Rating"));
				
				result.add(mt);
	            }
			}catch(Exception e) {
				e.printStackTrace();
			}
			return result;
	}
		
		
		
		
		
		public List<TopRatedMovies> getMoviesTopRated(){

			List<TopRatedMovies> result = new ArrayList<>();
	        String sql = "select * from topratedmovies";
	        try{
	            PreparedStatement ps = con.prepareStatement(sql);
	           
	            ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				TopRatedMovies tr = new TopRatedMovies();
				tr.setMovieId(rs.getInt("MovieId"));
				tr.setMovieTitle(rs.getString("MovieTitle"));
				tr.setYear(rs.getInt("Year"));
				tr.setStoryLine(rs.getString("storyline"));
				tr.setRating(rs.getInt("rating"));
				
				result.add(tr);
			}
	        }catch(Exception e) {
	        	e.printStackTrace();
	        	
	        }
			return result;
	}
		
		
		public List<TopRatedIndian> getMoviesIndian(){

			List<TopRatedIndian> result = new ArrayList<>();
	        String sql = "select * from topratedindian";
	        try{
	            PreparedStatement ps = con.prepareStatement(sql);
	           
	            ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				TopRatedIndian ti = new TopRatedIndian();
				ti.setMovieId(rs.getInt("MovieId"));
				ti.setMovieTitle(rs.getString("MovieTitle"));
				ti.setYear(rs.getInt("Year"));
				ti.setStoryLine(rs.getString("storyline"));
				ti.setRating(rs.getInt("rating"));
				
				result.add(ti);
			}
	        }catch(Exception e) {
	        	e.printStackTrace();
	        }
			return result;
	}
		
		

		
		public List<FavouriteMovies> getMoviesFavourite(){

			List<FavouriteMovies> result = new ArrayList<>();
	        String sql = "select * from favouritemovies";
	        try{
	            PreparedStatement ps = con.prepareStatement(sql);
	           
	            ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				FavouriteMovies fm = new FavouriteMovies();
				fm.setMovieId(rs.getInt("MovieId"));
				fm.setMovieTitle(rs.getString("MovieTitle"));
				fm.setYear(rs.getInt("Year"));
				fm.setStoryLine(rs.getString("storyline"));
				fm.setRating(rs.getInt("rating"));
				result.add(fm);
			}
	        }catch(Exception e) {
	        	e.printStackTrace();
	        }
			return result;
	}



}
